import styled from 'styled-components';
import { colors } from '@atlaskit/theme';

// tslint:disable-next-line:variable-name
const DateGroupHeader = styled.div`
  color: ${colors.N200};
  font-size: 12px;
  font-weight: 500;
  margin: 12px 0 4px 0;
`;

export default DateGroupHeader;
