# Task/Decision

This provides components for rendering tasks and decisions.

## Try it out

Interact with a [live demo of the @NAME@ component](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).

## Installation

```sh
npm install @NAME@
```

## Using the component

Use the component in your React app as follows:

```
import { DecisionList, DecisionItem } from '@NAME@';
ReactDOM.render(<DecisionItem>A decision</DecisionItem>, container);
ReactDOM.render(
  <DecisionList>
    <DecisionItem>A decision</DecisionItem>
    <DecisionItem>Another decision</DecisionItem>
  </DecisionList>
, container);
```
