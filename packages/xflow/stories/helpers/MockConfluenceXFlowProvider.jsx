import JiraToConfluenceXFlowProvider from '../../src/product-xflow-providers/JiraToConfluenceXFlowProvider';
import mockXFlowProviderFactory from './mockXFlowProviderFactory';

const MockConfluenceXFlowProvider = mockXFlowProviderFactory(JiraToConfluenceXFlowProvider);
export default MockConfluenceXFlowProvider;
