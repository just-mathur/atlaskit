
Though publicly available for reasons, this component is useless outside Atlassian.

For Atlassian users, search "@atlaskit/xflow private README" in the internal documentation.
