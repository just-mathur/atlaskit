import styled from 'styled-components';

const OptOutIcon = styled.span`
  padding: 0px 8px 0px 0px;
`;

OptOutIcon.displayName = 'OptOutIcon';
export default OptOutIcon;
